/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocktail;

/**
 *
 * @author MSi
 */
public class Fruits extends Ingredients{

    private double volum;
   private Color r;

    public Fruits(double volum, String name, double amount) {
        super(name, amount);
        this.volum = volum;
    }

    public Fruits() {
    }

    public Fruits(double volum) {
        this.volum = volum;
    }
    

    public double getVolum() {
        return volum;
    }

    public void setVolum(double volum) {
        this.volum = volum;
    }
    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
    public String getinfo()
    {
        return "te name "+ this.name+" "+this.amount+"\n";
    }
   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocktail;

/**
 *
 * @author MSi
 */
public abstract class Ingredients {
   protected String name;
    protected double amount;

    public Ingredients(String name, double amount) {
        this.name = name;
        this.amount = amount;
    }

    public Ingredients() {
    }
    
     public abstract String  getinfo();

    public abstract String getName();
     
    

    public abstract void setName(String name) ;
      

    public abstract double getAmount();
     

    public abstract void setAmount(double amount);
    
}

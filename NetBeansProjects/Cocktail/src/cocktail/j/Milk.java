/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocktail;

/**
 *
 * @author MSi
 */
public class Milk extends Ingredients{

    private double volum;
    private Color r;

    public Milk(double volum, String name, double amount) {
        super(name, amount);
        this.volum = volum;
    }

    public Milk() {
    }
    

    public double getVolum() {
        return volum;
    }

    public void setVolum(double volum) {
        this.volum = volum;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }
   public String getinfo()
   {
       return "the name "+this.name+"the amount "+this.amount;
   }
}

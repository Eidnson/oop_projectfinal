package cocktail;

public class Cup {

    private int capacity;

    private int calories;

    public Cup(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getCalories() {
        return calories;
    }
    
    public void setCalories(int calories){
        this.calories = calories;
    }
    
    
    
}

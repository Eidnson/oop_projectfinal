/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocktail;

/**
 *
 * @author Mohammed
 */
public class BlenderOverFlowException extends Exception{
    
    public BlenderOverFlowException(){
        super("Ingredients volume exceeds blender capacity");
    }
    
}

package cocktail;

public class Color implements java.io.Serializable{
    
    public Color(int r, int g, int b){
        this.r= r;
        this.g = g;
        this.b=  b;
    }

    private int r;

    private int g;

    private int b;

    public int getR() {
        return r;
    }

    public int getG() {
        return g;
    }

    public int getB() {
        return b;
    }
    
    
}

package cocktail;

public class Fruit extends Ingredient implements java.io.Serializable{

    private int volume;

    private Color color;

    
    
    

    public Fruit(String name, int calories, int volume, Color color) {
        super(name, calories);
        this.volume = volume;
        this.color = color;
    }

 
    public int getVolume() {
        return this.volume;
    }

    public Color getColor() {
        return this.color;
    }

   
}
